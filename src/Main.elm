module Main exposing (LightsLine, LightsTable, Msg(..), main, tableToggle, toggleElement)

import Html exposing (Html)
import Html.Attributes exposing (src, width)
import Html.Events exposing (onClick)


type alias LightsLine =
    List Bool


type Msg
    = Toggle Int


toggleElement : Int -> List Bool -> List Bool
toggleElement idx l =
    case ( l, idx ) of
        ( [], _ ) ->
            l

        ( e :: t, 0 ) ->
            not e :: t

        ( e :: t, i ) ->
            e :: toggleElement (idx - 1) t



---------------------- Lights Out -----------------------


type alias LightsTable =
    List LightsLine



-- type Msg = Toggle Int Int


tableToggle : Int -> Int -> LightsTable -> LightsTable
tableToggle idx idy table =
    case ( table, idx ) of
        ( [], _ ) ->
            table

        ( e :: t, 0 ) ->
            toggleElement idy e :: t

        ( e :: t, i ) ->
            e :: tableToggle (idx - 1) idy t


main : Html Msg
main =
    -- Browser.sandbox { init = ?, update = ?, view = ? }
    Html.pre [] [ Html.text "Who turned the lights off? " ]
