# Naloge v šestem tednu Elm@FRI

V tem tednu bomo našim programom dodali dinamiko.

## Naloga 1
Implementirajte vrstico lučk, ki se prižigajo in ugašajo s klikom miške.

![](ex1.png)

Podatkovni model za to aplikacijo predstavlja katere lučke so prižgane in katere niso. To predstavimo s seznamom Bool vrednosti.

```elm
type alias LightsLine = List Bool

```
Da se boste hitreje lotili bistva naloge, imate v Main datoteki že implementirano funkcijo

```elm
toggleElement : Int -> LightsLine -> LightsLine
```
ki sprejme indeks in seznam, ter negira vrednost v podanem seznamu na podanem indeksu.

Sporočila, ki jih pošiljajo Html elementi naj bodo oblike:

```elm
type Msg = Toggle Int
```
Vsaka lučka naj torej ob kliku pošlje svoj indeks, kar spremeni en element v podatkovnem modelu.

Da povežete vse komponente v dinamično aplikacijo, implementirajte metodi:

```elm
view : LightsLine -> Html Msg
update : Msg -> LightsLine -> LightsLine
```
in ju uporabite v metodi main.

## Naloga 2.


Druga naloga bo nadgradnja prve naloge. Implementirali boste znano igrico [ LightsOut ](https://en.wikipedia.org/wiki/Lights_Out_(game)).


Igralno polje je dvodimenzionalna tabela lučk, ki jo predstavimo z naslednjim tipom:

```elm
type alias LightsTable = List LightLine

```

V Main.elm imate že implementirano metodo za spreminjanje elementa na poziciji x, y v tabeli.

```elm
tableToggle : Int -> Int -> LightsTable -> LightsTable
```

Podatkovni tip za sporočila je v tem primeru razširjen še za eno koordinato:

```elm
type Msg = Toggle Int Int
```
Pri sprožitvi tega sporočila morate seveda spremeniti lučko na tej koordinati, pa še vse sosede te lučke.

Zopet implementirajte funkciji view in update in vse ustrezno povežite v funkciji main.


Primer igranja iger je prikazan na spodnji gibljivi sliki.

![](play.gif)
